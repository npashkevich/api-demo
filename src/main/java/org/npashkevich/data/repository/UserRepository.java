package org.npashkevich.data.repository;

import org.npashkevich.data.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Author: nikolai.pashkevich.
 */
public interface UserRepository extends JpaRepository<User, Long> {
    User findByEmail(String email);
}
