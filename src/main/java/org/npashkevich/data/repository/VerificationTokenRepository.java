package org.npashkevich.data.repository;

import org.npashkevich.data.entity.VerificationToken;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Author: nikolai.pashkevich.
 */
public interface VerificationTokenRepository extends JpaRepository<VerificationToken, Long> {
    VerificationToken findByToken(String token);
}
