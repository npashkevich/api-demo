package org.npashkevich.data.service;

import org.npashkevich.data.entity.VerificationToken;

/**
 * Author: nikolai.pashkevich.
 */
public interface VerificationTokenService {
    VerificationToken getVerificationToken(String token);
}
