package org.npashkevich.data.service;

import org.npashkevich.data.entity.VerificationToken;
import org.npashkevich.data.repository.VerificationTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Author: nikolai.pashkevich.
 */
@Service
public class VerificationTokenServiceImpl implements VerificationTokenService {

    private final VerificationTokenRepository tokenRepository;

    @Autowired
    public VerificationTokenServiceImpl(VerificationTokenRepository tokenRepository) {
        this.tokenRepository = tokenRepository;
    }

    @Override
    public VerificationToken getVerificationToken(final String token) {
        return tokenRepository.findByToken(token);
    }
}
