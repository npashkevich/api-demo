package org.npashkevich.data.service;

import org.npashkevich.api.entity.UserDto;
import org.npashkevich.data.entity.User;
import org.npashkevich.data.service.error.UserAlreadyExistException;

/**
 * Author: nikolai.pashkevich.
 */
public interface UserService {
    User registerNewUserAccount(UserDto userDto) throws UserAlreadyExistException;
    void createVerificationTokenForUser(User user, String token);
    void saveRegisteredUser(final User user);
}
