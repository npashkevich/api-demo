package org.npashkevich.api.validator;

import org.npashkevich.api.entity.UserDto;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Author: nikolai.pashkevich.
 */
@Component
public class PhoneValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return UserDto.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object o, Errors errors) {
        UserDto userDto = (UserDto) o;
        if (userDto.getMobilePhoneNumber() == null && userDto.getLandlinePhoneNumber() == null) {
            errors.rejectValue("mobilePhoneNumber", "phone.required", "One of the phone fields required");
            errors.rejectValue("landlinePhoneNumber", "phone.required", "One of the phone fields required");
        }
    }
}
