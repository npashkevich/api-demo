package org.npashkevich.api;

import org.npashkevich.api.error.ExpiredVerificationTokenException;
import org.npashkevich.api.error.InvalidVerificationTokenException;
import org.npashkevich.api.registration.OnRegistrationCompleteEvent;
import org.npashkevich.api.validator.PhoneValidator;
import org.npashkevich.data.entity.User;
import org.npashkevich.api.entity.UserDto;
import org.npashkevich.data.entity.VerificationToken;
import org.npashkevich.data.service.UserService;
import org.npashkevich.api.error.InvalidRequestException;
import org.npashkevich.data.service.VerificationTokenService;
import org.npashkevich.data.service.error.UserAlreadyExistException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Calendar;

/**
 * Author: nikolai.pashkevich.
 */
@Controller
@RequestMapping(path = "/users")
public class RegistrationController {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    private final ApplicationEventPublisher eventPublisher;
    private final UserService userService;
    private final VerificationTokenService verificationTokenService;
    private final PhoneValidator phoneValidator;

    @Autowired
    public RegistrationController(UserService userService, ApplicationEventPublisher eventPublisher,
                                  VerificationTokenService verificationTokenService, PhoneValidator phoneValidator) {
        this.userService = userService;
        this.eventPublisher = eventPublisher;
        this.verificationTokenService = verificationTokenService;
        this.phoneValidator = phoneValidator;
    }

    @ResponseBody
    @RequestMapping(value = {"/registration", "/registration/"}, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.POST)
    public ResponseEntity registerUserAccount(
            @Valid @RequestBody final UserDto userDto, BindingResult bindingResult, HttpServletRequest request)
            throws UserAlreadyExistException {

        LOGGER.debug("Registering user account with information: {}", userDto);
        phoneValidator.validate(userDto, bindingResult);
        if (bindingResult.hasErrors()) {
            throw new InvalidRequestException("Invalid format of user object", bindingResult);
        }
        final User registered = userService.registerNewUserAccount(userDto);
        eventPublisher.publishEvent(new OnRegistrationCompleteEvent(registered, getAppUrl(request)));

        return new ResponseEntity<>(userDto, HttpStatus.OK);
    }

    @ResponseBody
    @RequestMapping(value = {"/registrationConfirm", "/registrationConfirm/"}, method = RequestMethod.GET)
    public ResponseEntity confirmRegistration(
            final HttpServletRequest request, @RequestParam("token") final String token)
            throws InvalidVerificationTokenException, ExpiredVerificationTokenException {

        final VerificationToken verificationToken = verificationTokenService.getVerificationToken(token);
        if (verificationToken == null) {
            throw new InvalidVerificationTokenException("Invalid verification token: " + token);
        }

        final User user = verificationToken.getUser();
        if (user.isEnabled()) {
            return new ResponseEntity<>("Account already verified", HttpStatus.OK);
        } else {
            final Calendar cal = Calendar.getInstance();
            if ((verificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
                throw new ExpiredVerificationTokenException("Expired verification token: " + token);
            }

            user.setEnabled(true);
            userService.saveRegisteredUser(user);
            return new ResponseEntity<>("Account successfully verified", HttpStatus.OK);
        }
    }

    private String getAppUrl(HttpServletRequest request) {
        return request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
    }
}
