package org.npashkevich.api.error;

import org.springframework.validation.Errors;

/**
 * Author: nikolai.pashkevich.
 */
@SuppressWarnings("serial")
public class InvalidRequestException extends RuntimeException {
    private Errors errors;

    public InvalidRequestException(String message, Errors errors) {
        super(message);
        this.errors = errors;
    }

    public Errors getErrors() {
        return errors;
    }
}
