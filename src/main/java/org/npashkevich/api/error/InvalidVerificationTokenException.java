package org.npashkevich.api.error;

/**
 * Author: nikolai.pashkevich.
 */
public class InvalidVerificationTokenException extends RuntimeException {
    public InvalidVerificationTokenException(final String message) {
        super(message);
    }
}
