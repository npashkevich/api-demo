package org.npashkevich.api.error;

/**
 * Author: nikolai.pashkevich.
 */
public class ExpiredVerificationTokenException extends RuntimeException {
    public ExpiredVerificationTokenException(final String message) {
        super(message);
    }
}
