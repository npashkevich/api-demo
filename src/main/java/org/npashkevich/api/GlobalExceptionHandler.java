package org.npashkevich.api;

import org.npashkevich.api.entity.error.ErrorResource;
import org.npashkevich.api.entity.error.FailureResponse;
import org.npashkevich.api.entity.error.FieldErrorResource;
import org.npashkevich.api.error.ExpiredVerificationTokenException;
import org.npashkevich.api.error.InvalidRequestException;
import org.npashkevich.api.error.InvalidVerificationTokenException;
import org.npashkevich.data.service.error.UserAlreadyExistException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Author: nikolai.pashkevich.
 */
@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    private static final String INVALID_REQUEST_CODE = "InvalidRequest";
    private static final String USER_ALREADY_EXISTS_CODE = "UserAlreadyExists";
    private static final String INVALID_TOKEN_CODE = "InvalidVerificationToken";
    private static final String EXPIRED_TOKEN_CODE = "ExpiredVerificationToken";

    private final HttpHeaders httpHeaders;

    public GlobalExceptionHandler() {
        this.httpHeaders = new HttpHeaders();
        this.httpHeaders.setContentType(MediaType.APPLICATION_JSON);
    }

    @ExceptionHandler(InvalidRequestException.class)
    private ResponseEntity handleInvalidRequest(RuntimeException e, WebRequest request) {
        InvalidRequestException ire = (InvalidRequestException) e;
        List<FieldErrorResource> fieldErrorResources = new ArrayList<>();

        List<FieldError> fieldErrors = ire.getErrors().getFieldErrors();
        fieldErrorResources
                .addAll(fieldErrors
                        .stream()
                        .map(fieldError ->
                                new FieldErrorResource(fieldError.getField(), fieldError.getDefaultMessage()))
                        .collect(Collectors.toList()));

        ErrorResource error = new ErrorResource(INVALID_REQUEST_CODE, ire.getLocalizedMessage());
        error.setFieldErrors(fieldErrorResources);

        return handleExceptionInternal(e, error, this.httpHeaders, HttpStatus.UNPROCESSABLE_ENTITY, request);
    }

    @ExceptionHandler(UserAlreadyExistException.class)
    private ResponseEntity handleUserAlreadyExist(RuntimeException e, WebRequest request) {
        UserAlreadyExistException ulee = (UserAlreadyExistException) e;
        FailureResponse error = new FailureResponse(USER_ALREADY_EXISTS_CODE, ulee.getLocalizedMessage());

        return handleExceptionInternal(ulee, error, this.httpHeaders, HttpStatus.UNPROCESSABLE_ENTITY, request);
    }

    @ExceptionHandler(InvalidVerificationTokenException.class)
    private ResponseEntity handleInvalidVerificationToken(RuntimeException e, WebRequest request) {
        InvalidVerificationTokenException ivte = (InvalidVerificationTokenException) e;
        FailureResponse error = new FailureResponse(INVALID_TOKEN_CODE, ivte.getLocalizedMessage());

        return handleExceptionInternal(ivte, error, this.httpHeaders, HttpStatus.OK, request);
    }

    @ExceptionHandler(ExpiredVerificationTokenException.class)
    private ResponseEntity handleExpiredVerificationToken(RuntimeException e, WebRequest request) {
        ExpiredVerificationTokenException evte = (ExpiredVerificationTokenException) e;
        FailureResponse error = new FailureResponse(EXPIRED_TOKEN_CODE, evte.getLocalizedMessage());

        return handleExceptionInternal(evte, error, this.httpHeaders, HttpStatus.OK, request);
    }
}
